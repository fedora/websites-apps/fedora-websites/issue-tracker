# Design feedback

This is the issue tracker regarding design for [fedora-websites-3.0].

If you find a problem, please [check if there is an existing issue], otherwise [create an issue].

[fedora-websites-3.0]: https://gitlab.com/fedora/websites-apps/fedora-websites/fedora-websites-3.0
[check if an existing issue]: https://gitlab.com/fedora/websites-apps/fedora-websites/design-feedback/-/issues
[create an issue]: https://gitlab.com/fedora/websites-apps/fedora-websites/design-feedback/-/issues/new
